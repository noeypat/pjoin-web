import moment from 'moment';

const convertDateToTh = (date, onlyDate, formatDate = "DD MM YY") => {
    return `${moment(date).add(543,"years").format(formatDate)} ${onlyDate ? moment(date).format("HH:mm") : "" }`
  }

  

  export {
    convertDateToTh,
  }
  