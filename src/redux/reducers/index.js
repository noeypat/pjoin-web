import {combineReducers} from 'redux'
import cv from './cv'
import job from './job'
import address from './address'

const rootReducer = combineReducers({
    cv,
    job,
    address
})
export default rootReducer;