import {
    GET_PROVINCES,
    GET_AUMPHURS,
    GET_DISTRICTS
 
} from '../action/address'

let initialState = {
    provinces:[],
    aumphurs:[],
    districts:[]
}



export default function (state = initialState, action) {

    const { type, payload } = action
    
    switch (type) {
        case GET_PROVINCES: {
            return {...state,
                provinces: payload
            }
        }

        case GET_AUMPHURS: {
            return {...state,
                aumphurs: payload
            }
        }

       case GET_DISTRICTS:{
        return {...state,
            districts: payload
            }
        }

        default: {
            return state
        }
    }

    

}