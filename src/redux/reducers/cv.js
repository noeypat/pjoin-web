import {
    LOGIN_CV,
    LOADING_CV,
    SET_CV
} from '../action/cv'

let initialState = {
    cv: {}
}

export default function (state = initialState, action) {

    const { type, payload } = action
    
    switch (type) {
        case LOGIN_CV: {
            return {
                ...state,
                cv: payload
            }
        }

        case LOADING_CV: {
            return {
                ...state,
                cv: payload
            }
        }

        case SET_CV: {
            return {
                ...state,
                cv: payload
            }
        }

        default: {
            return state
        }
    }

}
// End function