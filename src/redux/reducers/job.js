import {
    GET_JOB,
 
} from '../action/job'

let initialState = []

export default function (state = initialState, action) {

    const { type, payload } = action
    
    switch (type) {
        case GET_JOB: {
            return payload
        }

        default: {
            return state
        }
    }

    

}
// End function