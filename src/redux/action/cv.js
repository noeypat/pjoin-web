import axios from "axios";
import api from '../../config/api'


export const LOGIN_CV = 'LOGIN_CV'
export const LOADING_CV = 'LOADING_CV'
export const SET_CV = 'SET_CV'



const login = (email, code) => async (dispatch) => {
    

    try {
        let user = await axios.post(`${api.host}/code/VerifyCode`, {
            Email: email,
            Code: code
        })


        window.localStorage.setItem('myData',JSON.stringify(user.data.body) );
        window.location.href = "/joblist"

        dispatch({
            type: LOGIN_CV,
            payload: user.data.body
        })
    }catch (err) {
    console.log(err);

}
}

const loading = () => async (dispatch) => {
    
    let cvData= null
    try {
        // let user = await axios.get(`${api.host}/cv`)
        try{
            cvData = JSON.parse(window.localStorage.getItem('myData')) 
        }catch(err){
            console.log(err);
            
        }

        dispatch({
            type: LOADING_CV,
            payload: cvData 
        })

    }catch (err) {
    console.log(err);

    }
}

const updateCV = (updateData,userID,pathName) => async (dispatch) => {

    try {
        let user = await axios.patch(`${api.host}/cv/${userID}`,updateData)

        window.localStorage.setItem('myData',JSON.stringify(user.data.body));

        if(pathName != null){
          window.location.href = `/${pathName}`  
        }
        
        dispatch({
            type: SET_CV,
            payload: user.data.body
        })
        console.log(user);
        
    }catch (err) {
    console.log(err);

}

}

export {
    login,
    loading,
    updateCV
}
