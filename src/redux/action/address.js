import axios from "axios";
import api from '../../config/api'

export const GET_PROVINCES = 'GET_PROVINCES'
export const GET_AUMPHURS = 'GET_AUMPHURS'
export const GET_DISTRICTS= 'GET_DISTRICTS'

const provincesList = () => async (dispatch) => {

    //TODO: call api ,resault,setState
    let provinces = await axios.get(`${api.host}/provinces`)

    
    dispatch({
        type: GET_PROVINCES,
        payload: provinces.data.body
    })
}

const aumphursList = (ID) => async (dispatch) => {

    //TODO: call api ,resault,setState
    let aumphurs = await axios.get(`${api.host}/aumphurs/province/${ID}`)

    
    dispatch({
        type: GET_AUMPHURS,
        payload: aumphurs.data.body
    })
}

const districtsList = (ID) => async (dispatch) => {

    //TODO: call api ,resault,setState
    let districts = await axios.get(`${api.host}/districts/amphure/${ID}`)
    
    dispatch({
        type: GET_DISTRICTS,
        payload: districts.data.body
    })
}

export {
    provincesList,
    aumphursList,
    districtsList
}
