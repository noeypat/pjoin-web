import axios from "axios";
import api from '../../config/api'

export const GET_JOB = 'GET_JOB'


const jobList = () => async (dispatch) => {

    //TODO: call api ,resault,setState
    let job = await axios.get(`${api.host}/job`)

    
    dispatch({
        type: GET_JOB ,
        payload: job.data.body
    })
}


export {
    jobList,

}
