import React from 'react'
import logoInformation from '../img/AW-Information.png'
import '../style/information.css'
import { Collapse} from 'reactstrap';
//import { RadioGroup, RadioButton } from 'react-radio-buttons';
import { connect } from 'react-redux'
import { loading,updateCV } from '../redux/action/cv'

class userInformation extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            idCard: "",
            race: "",
            nation: "",
            religion: "",
            gender: "",
            militaryStatus: "",
            maritalStatus: "",
            isLoading: false,
            
            collapseText:{"race": false,"nation": false,"religion": false}
         
        }
       
    }

    async componentDidMount() {
        await this.props.loadCV()

        if(this.props.cv.cv != "")
        {
          await  this.setState({
                idCard: this.props.cv.cv.IDCard,
                race: this.props.cv.cv.Race,
                nation: this.props.cv.cv.Nation,
                religion: this.props.cv.cv.Religion,
                gender: this.props.cv.cv.Gender,
                militaryStatus: this.props.cv.cv.MilitaryStatus,
                maritalStatus: this.props.cv.cv.MaritalStatus,
                isLoading: true,
            })
        }
        
        
    }

    handleChangeidCard = evt => {
        this.setState({
            idCard: evt.target.value
        })
    }

    onChangeRace = evt => {
        if(evt.target.value == "อื่นๆ"){
            this.setState({
                race: "",
                collapseRace: !this.state.collapseRace
            }) 
        }
        else{
           this.setState({
                race: evt.target.value
        }) 
        }
    }

    onChangeRadio = evt => {
        let name = evt.target.name
   
        
        if(evt.target.value == "อื่นๆ"){
            this.setState(prevState =>{
                prevState[name] = ""
                prevState.collapseText[name] = true
                return prevState
                
            }) 
        }
        else{
           this.setState({
                [evt.target.name]: evt.target.value
        }) 
        }
    }

    onChangeText = evt => {
        this.setState({
            [evt.target.name]: evt.target.value
    }) 
    }

    handleSummit = async () =>{
       
        if(this.state.militaryStatus == "ได้รับการยกเว้น")
        {
          await  this.setState({
            militaryStatus: "SPARE"
        }) 
        }

        if(this.state.militaryStatus == "ปลดเป็นทหารกองหนุน")
        {
          await this.setState({
            militaryStatus: "PASSED"
        }) 
        }

        if(this.state.militaryStatus == "ยังไม่ได้รับการเกณฑ์ทหาร")
        {
          await this.setState({
            militaryStatus: "EX"
        }) 
        }

        if(this.state.maritalStatus == "โสด")
        {
          await this.setState({
            maritalStatus: "SINGLE"
        }) 
        }

        if(this.state.maritalStatus == "แต่งงาน")
        {
          await this.setState({
            maritalStatus: "MARRIED"
        }) 
        }
        let pathName = "address"
        this.props.updateCV(this.state,this.props.cv.cv.id,pathName)
    }

    Cancel = () => {
        window.location.href ="/createuser"
    } 

    render() {
        if(!this.state.isLoading){
            return null
        }
      
        return (
            <div className="BG">
                <div className="card-center">
                    <div className="card-body">
                        <h5>ทำความรู้จักกันก่อน</h5>
                        <img className="logo" src={logoInformation} alt="logoInformation" />
                    </div>
                </div>

                <form>
                    <div className="form-group">
                        <label>เลขที่บัตรประชาชน/หมายเลขหนังสือเดินทาง<span>*</span></label>
                        <input className="form-control"
                            type="text"
                            value={this.state.idCard}
                            onChange={this.handleChangeidCard}
                        />
                    </div>
                </form>
                    <div className="select-info">
                        <label>เชื้อชาติ</label>
                        <div className = "">
                            <label className="checkbox-inline">
                                <input className="checkbox-infor"
                                    name = "race"
                                    type="radio"
                                    value="ไทย"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.race == "ไทย" ? true:false}
                                />
                                ไทย
                        </label>
                               <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "race"
                                    type="radio" 
                                    value="อื่นๆ"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                              
                                    />
                                    อื่นๆ
 
                                </label> 

                                <Collapse className="collapse-show" isOpen={this.state.collapseText["race"]}>
                                    <input type="text"
                                    name = "race"
                                    className="form-control"
                                    value={this.state.race}
                                    onChange ={(e) =>this.onChangeText(e)}
                                    />
                                </Collapse>   
                            </div>
                        </div>

                <div className="select-info">
                        <label>สัญชาติ</label>
                        <div className = "">
                            <label className="checkbox-inline">
                                <input className="checkbox-infor"
                                    name = "nation"
                                    type="radio"
                                    value="ไทย"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.nation == "ไทย" ? true:false}
                                />
                                ไทย
                        </label>
                               <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "nation"
                                    type="radio" 
                                    value="อื่นๆ"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.nation != "ไทย" ? true:false}
                                    />
                                    อื่นๆ
                                </label> 
                                <Collapse className="collapse-show" isOpen={this.state.collapseText["nation"]}>
                                <input 
                                    type="text"
                                    name="nation"
                                    className="form-control"
                                    value={this.state.nation}
                                    onChange ={(e) =>this.onChangeText(e)}
                                />
                                </Collapse>   

                               
                        </div>
                </div>

                <div className="select-info">
                        <label>ศาสนา</label>
                        <div className = "">
                            <label className="checkbox-inline">
                                <input className="checkbox-infor"
                                    name = "religion"
                                    type="radio"
                                    value="พุทธ"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.religion == "พุทธ" ? true:false}
                                />
                                พุทธ
                        </label>
                               <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "religion"
                                    type="radio" 
                                    value="อื่นๆ"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.religion != "พุทธ" ? true:false}
                                    />
                                    อื่นๆ
                                </label> 
                                <Collapse className="collapse-show" isOpen={this.state.collapseText["religion"]}>
                                <input 
                                    type="text"
                                    name="religion"
                                    className="form-control"
                                    value={this.state.religion}
                                    onChange ={(e) =>this.onChangeText(e)}
                                />
                                </Collapse>   
                        </div>
                </div>

                <div className="select-info">
                        <label>เพศ</label>
                        <div className = "">
                            <label className="checkbox-inline">
                                <input className="checkbox-infor"
                                    name = "gender"
                                    type="radio"
                                    value="ชาย"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.gender == "ชาย" ? true:false}
                                />
                                ชาย
                        </label>
                               <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "gender"
                                    type="radio" 
                                    value="หญิง"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.gender == "หญิง" ? true:false}
                                    />
                                    หญิง
                                </label> 
                                <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "gender"
                                    type="radio" 
                                    value="LGBT"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked = { this.state.gender == "LGBT" ? true:false}
                                    />
                                    LGBT
                                </label> 
                        </div>
                </div>

                <div className="select-info">
                        <label>ภาวะทางทหาร</label>
                        <div className = "select-militaryStatus">
                            <label className="checkbox-inline">
                                <input className="checkbox-infor"
                                    name = "militaryStatus"
                                    type="radio"
                                    value="ได้รับการยกเว้น"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked={this.state.militaryStatus == "SPARE" ? true:false}
                                />
                                ได้รับการยกเว้น
                        </label>
                               <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "militaryStatus"
                                    type="radio" 
                                    value="ปลดเป็นทหารกองหนุน"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked={this.state.militaryStatus == "PASSED" ? true:false}
                                    />
                                    ปลดเป็นทหารกองหนุน
                                </label> 
                                <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "militaryStatus"
                                    type="radio" 
                                    value="ยังไม่ได้รับการเกณฑ์ทหาร"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked={this.state.militaryStatus == "EX" ? true:false}
                                    />
                                    ยังไม่ได้รับการเกณฑ์ทหาร
                                </label> 
                        </div>
                </div>

                <div className="select-info">
                        <label>สถานภาพ</label>
                        <div className = "">
                            <label className="checkbox-inline">
                                <input className="checkbox-infor"
                                    name = "maritalStatus"
                                    type="radio"
                                    value="โสด"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked={this.state.maritalStatus == "SINGLE" ? true:false}
                                />
                                โสด
                        </label>
                               <label className="checkbox-inline">
                                <input className="checkbox-infor" 
                                    name = "maritalStatus"
                                    type="radio" 
                                    value="แต่งงาน"
                                    onChange ={(e) =>this.onChangeRadio(e)}
                                    defaultChecked={this.state.maritalStatus == "MARRIED" ? true:false}
                                    />
                                    แต่งงาน
                                </label> 
                               
                        </div>
                </div>
                <div className="btn-center">
                    <button type="button" className="btn btn-resubmit" onClick={this.Cancel} >ย้อนกลับ</button>
                    <button type="button" className="btn btn-submit" onClick={this.handleSummit}>ลุยต่อหน้าถัดไป</button>
                </div>

            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
       
        loadCV: () => dispatch(loading()) ,
        updateCV: (updateData,userID,pathName) => dispatch(updateCV(updateData,userID,pathName))

    }
}

const mapStateToProps = reduxState => {
    return {
        cv: reduxState.cv
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(userInformation)