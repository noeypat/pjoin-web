import React from 'react'
import '../style/ContactPerson.css'
import logoContactPerson from "../img/AW-ContactPerson.png"
import { FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux'
import { loading,updateCV } from '../redux/action/cv'



class ContactPerson extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            tel: "",
            address: "",
            district: "",
            provincesName: "",
            aumphursName: "",
            districtName: "",
            zipcode: "",
           // parentList: [{name: ""}]
            parentList:["บิดา","มารดา"],
            parent: "",
            checkBox: false
        }
    }

    async componentDidMount() {
        await this.props.loadCV()
    }

    handleChangeName = evt => {
        this.setState({
            name: evt.target.value
        })
    }

    handleChangeTel = evt => {
        this.setState({
            tel: evt.target.value
        })
    }

    selectParent = (e) => {
        this.setState({
            parent: e.target.value
        })
    }

    selectCheckbox = () => {
        this.setState({
            checkBox: !this.state.checkBox,
            provincesName: this.props.cv.cv.Provinces,
            aumphursName: this.props.cv.cv.Aumphurs,
            districtName: this.props.cv.cv.District,
            zipcode: this.props.cv.cv.zipCode,
        })
    }

    handleChangeAddress = evt => {
        this.setState({
            address: evt.target.value
        })
    }

    handleChangeDistrict = evt => {
        this.setState({
            district: evt.target.value
        })
    }

    render() {
        return (
            <div className="BG">
                <div className="card-center">
                    <div className="card-body">
                        <h3>บุคคลที่ติดต่อกรณีฉุกเฉิน</h3>
                        <img className="logo" src={logoContactPerson} alt="logoContactPerson" />
                    </div>
                </div>
                <form>
                    <div className="form-group col-md-4">
                        <label>ชื่อ - สกุล<span>*</span></label>
                        <input className="form-control"
                            type="text"
                            value={this.state.name}
                            onChange={this.handleChangeName}
                        />
                    </div>
                    <div className="form-group col-md-4">
                        <label>เบอร์โทรศัพท์<span>*</span></label>
                        <input className="form-control"
                            type="text"
                            value={this.state.tel}
                            onChange={this.handleChangeTel}
                        />
                    </div>

                    <div className="form-group col-md-4">
                        <label>เกี่ยวข้องเป็น<span>*</span></label>
                        <select className="form-control"
                            name="selectJob"
                            onChange={(e) => this.selectParent(e)}
                            value={this.state.parent}
                        >
                            <option></option>
                            {
                            this.state.parentList.map((item, index) => {
                                return (
                                    <option key={index} value={item}>{item}</option>
                                )
                            })
                        }
                        </select>
                        </div>
                        <FormGroup check>
                            <Label check>
                                <Input type="checkbox" onClick= {this.selectCheckbox}/>{' '}
                                <h6>ใช้ที่อยู่เดียวกันกับ “ที่อยู่ปัจจุบันของคุณ”</h6>
                            </Label>
                        </FormGroup>
                        <div className="form-group col-md-4">
                            <h6>เลขที่ / หมู่ที่ / ซอย <br/>(กรอกรายละเอียดเพิ่มเติมเช่น ชื่อ / ชั้น / ห้อง)<span>*</span></h6>
                            <input className="form-control"
                            type="text"
                            value={this.state.address}
                            onChange={this.handleChangeAddress}
                            />
                    </div>
                    <div className="form-group col-md-4">
                        <label>ถนน<span>*</span></label>
                        <input className="form-control"
                            type="text"
                            value={this.state.district}
                            onChange={this.handleChangeDistrict}
                        />
                    </div>
                    
                </form>
           
                        <div className="form-select">
                            <label>จังหวัด<span>*</span></label>
                            <select className="form-control form-selectjob"
                                name="Provinces"
                                onChange={(e) => this.selectProvinces(e)}
                                value={this.state.provincesID}
                            >
                                <option></option>
                                {/* {
                                    this.props.provincesList.map((item, index) => {
                                        return (
                                            <option key={index} value={item.id}>{item.nameTh}</option>
                                        )
                                    })
                                } */}
                            </select>
                        </div>

                        <div className="form-select">
                            <label>อำเภอ / เขต<span>*</span></label>
                            <select className="form-control form-selectjob"
                                name="Aumphurs"
                                onChange={(e) => this.selectAumphurs(e)}
                                value={this.state.aumphursID}
                            >
                                <option></option>
                                {/* {
                                    this.props.aumphursList.map((item, index) => {
                                        return (
                                            <option key={index} value={item.id}>{item.nameTh}</option>
                                        )
                                    })
                                } */}
                            </select>
                        </div>

                        <div className="form-select">
                            <label>ตำบล / แขวง<span>*</span></label>
                            <select className="form-control form-selectjob"
                                name="Districts" 
                                onChange={(e) => this.selectDistricts(e)}
                                value={this.state.districtsID}
                            >
                                <option></option>
                                {/* {
                                    this.props.districtsList.map((item, index) => {
                                        return (
                                            <option key={index} value={item.id}>{item.nameTh}</option>
                                        )
                                    })
                                } */}
                            </select>
                        </div>

                        <div className="form-group col-md-4 ">
                        <label>รหัสไปรษณีย์<span>*</span></label>
                        <input className="form-control"
                            type="text"
                            name="Zipcode"
                            value={this.state.zipcode}
                            onChange = {this.onChangeZipcode}
                        />
                    </div>

                <div className="btn-center">
                    <button type="button" className="btn btn-resubmit" onClick={this.Cancel} >ย้อนกลับ</button>
                    <button type="button" className="btn btn-submit" onClick={this.handleSummit}>ลุยต่อหน้าถัดไป</button>
                </div>

            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
       
        loadCV: () => dispatch(loading()) ,
     //   updateCV: (updateData,userID,pathName) => dispatch(updateCV(updateData,userID,pathName))

    }
}

const mapStateToProps = reduxState => {
    return {
        cv: reduxState.cv
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactPerson)