import React from 'react'
import '../style/address.css'
import logoAddress from "../img/AW-Address.png"
import { connect } from 'react-redux'
import { provincesList, aumphursList, districtsList } from '../redux/action/address'
import { loading, updateCV } from '../redux/action/cv'

class Address extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            owner: "",
            address: "",
            district: "",
            provincesName: "",
            provincesID: "",
            aumphursName: "",
            aumphursID: "",
            districtsName:"",
            districtsID: "",
            zipcode:""
        
        }
    }

    async componentDidMount() {
        await this.props.loadCV()
        await this.props.Provinces() // name same mapDispatchToProps
    
    }

    onChangeOwner = evt => {
        this.setState({
            owner: evt.target.value
        })
    }

    onChangeAddress = evt => {
        this.setState({
            address: evt.target.value
        })
    }

    onChangeDistrict = evt => {
        this.setState({
            district: evt.target.value
        })
    }

    selectProvinces = async (e) => {
        let indexPovinces = e.target.value
        
        let targetPovinces = this.props.provincesList.filter((item)=>{
            return item.id == indexPovinces
        })

        await this.setState({
            provincesName: targetPovinces[0].nameTh,
            provincesID: targetPovinces[0].id
        })
        
       await this.props.Aumphurs(this.state.provincesID)
    }

    selectAumphurs = async (e) => {
        
        let indexDistricts = e.target.value
        
        let targetAumphurs = this.props.aumphursList.filter((item)=>{
            return item.id == indexDistricts
        })

        await this.setState({
            aumphursName: targetAumphurs[0].nameTh,
            aumphursID: targetAumphurs[0].id
        })
        await this.props.Districts(indexDistricts)
    }

    selectDistricts= async (e) => {
        
        let indexDistricts = e.target.value
        
        let targetDistricts = this.props.districtsList.filter((item)=>{
            return item.id == indexDistricts
        })

        await this.setState({
            districtsName: targetDistricts[0].nameTh,
            districtsID: targetDistricts[0].id
        })
        
    }

    onChangeZipcode = evt => {
        this.setState({
            zipcode: evt.target.value
        })
    }

    Cancel = () => {
        window.location.href ="/information"
    }

    handleSummit = () =>{

        let updateData = {
            Owner: this.state.owner,
            Address1: this.state.address,
            Province: this.state.provincesName,
            Aumphur: this.state.aumphursName,
         //   District: this.state.districtsName,
            ZipCode: this.state.zipcode
        }
        console.log(updateData);
        
        let pathName = "contactperson"
        this.props.updateCV(updateData,this.props.cv.cv.id,pathName)
    }

    render() {
        return (
            <div className="BG">
                <div className="card-center">
                    <div className="card-body">
                        <h5>ที่อยู่ปัจจุบันของคุณ</h5>
                        <img className="logo" src={logoAddress} alt="logoAddress" />
                    </div>
                </div>
                <div className="card-address">
                    <div className="form-address">
                        <h6>ลักษณะที่อยู่อาศัย</h6>
                        <label className="checkbox-inline">
                            <input
                                name="ower"
                                type="radio"
                                value="OWNER"
                                onChange={this.onChangeOwner}
                            />
                            <h6>เจ้าของที่อยู่อาศัย<br />( ของตนเอง / พ่อเเม่ / ญาติพี่น้อง )</h6>
                        </label>

                        <label className="checkbox-inline">
                            <input
                                name="ower"
                                type="radio"
                                value="RENTAL"
                                onChange={this.onChangeOwner}
                            />
                            <h6>เช่า ( บ้าน / คอนโด / หอพัก ฯลฯ )</h6>
                        </label>

                        <h6>เลขที่ / หมู่ที่ / ซอย<br />(กรอกรายละเอียดเพิ่มเติมเช่น ชื่อ / ชั้น / ห้อง)<span>*</span></h6>
                        <textarea
                            type="text"
                            name="address"
                            value={this.state.address}
                            onChange={this.onChangeAddress}
                        ></textarea>

                        <h6>ถนน<span>*</span></h6>
                        <input
                            type="text"
                            name="district"
                            value={this.state.district}
                            onChange={this.onChangeDistrict}
                        >
                        </input>

                        <div className="form-select">
                            <label>จังหวัด<span>*</span></label>
                            <select className="form-control form-selectjob"
                                name="Provinces"
                                onChange={(e) => this.selectProvinces(e)}
                                value={this.state.provincesID}
                            >
                                <option></option>
                                {
                                    this.props.provincesList.map((item, index) => {
                                        return (
                                            <option key={index} value={item.id}>{item.nameTh}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>

                        <div className="form-select">
                            <label>อำเภอ / เขต<span>*</span></label>
                            <select className="form-control form-selectjob"
                                name="Aumphurs"
                                onChange={(e) => this.selectAumphurs(e)}
                                value={this.state.aumphursID}
                            >
                                <option></option>
                                {
                                    this.props.aumphursList.map((item, index) => {
                                        return (
                                            <option key={index} value={item.id}>{item.nameTh}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>

                        <div className="form-select">
                            <label>ตำบล / แขวง<span>*</span></label>
                            <select className="form-control form-selectjob"
                                name="Districts" 
                                onChange={(e) => this.selectDistricts(e)}
                                value={this.state.districtsID}
                            >
                                <option></option>
                                {
                                    this.props.districtsList.map((item, index) => {
                                        return (
                                            <option key={index} value={item.id}>{item.nameTh}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>

                        <h6>รหัสไปรษณีย์<span>*</span></h6>
                        <input
                            type="text"
                            name="Zipcode"
                            value={this.state.zipcode}
                            onChange = {this.onChangeZipcode}
                        >
                        </input>
                   
                        <div className="btn-center">
                            <button type="button" className="btn btn-resubmit" onClick={this.Cancel}>ย้อนกลับ</button>
                            <button type="button" className="btn btn-submit" onClick={this.handleSummit}>ลุยต่อหน้าถัดไป</button>
                        </div>

               

                </div>
            </div>
            </div>
        )
    }

}

const mapDispatchToProps = dispatch => {
    return {
        loadCV: () => dispatch(loading()),
        updateCV: (updateData, userID, pathName) => dispatch(updateCV(updateData, userID, pathName)),

        Provinces: () => dispatch(provincesList()), // provincesList in action
        Aumphurs: (ID) => dispatch(aumphursList(ID)),
        Districts: (ID) => dispatch(districtsList(ID)),

    }
}

const mapStateToProps = reduxState => {
    return {
        cv: reduxState.cv,
        provincesList: reduxState.address.provinces,
        aumphursList: reduxState.address.aumphurs,
        districtsList: reduxState.address.districts,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Address)