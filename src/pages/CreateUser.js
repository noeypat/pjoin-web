import React from 'react'
import '../style/joblist.css'
import logoCreateUser from '../img/AW-CreateUser.png'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css"
import { connect } from 'react-redux'
import { loading,updateCV } from '../redux/action/cv'
import moment from 'moment';

class CreateUser extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            nickname: "",
            fullname: "",
            tel: "",
            email: "",
            birthday: new Date(),
            hight: "",
            weight: ""
        }
    }

   async componentDidMount() {
        await this.props.loadCV()
          this.setState({
            email: this.props.cv.cv.Email
        })

        if(this.props.cv.cv.nickname != "")
        {
            this.setState({
                nickname: this.props.cv.cv.NickName,
                fullname: this.props.cv.cv.Name,
                tel: this.props.cv.cv.Mobile,
                hight: this.props.cv.cv.Height,
                weight: this.props.cv.cv.Weight
            })

        }
    }

    handleChangeNickname = evt => {
        this.setState({
            nickname: evt.target.value
        })
    }

    handleChangeFullname = evt => {
        this.setState({
            fullname: evt.target.value
        })
    }

    handleChangeTel = evt => {
        this.setState({
            tel: evt.target.value
        })
    }

    handleChangeBirthday = date => {
        this.setState({
            birthday: date
        })
    }

    handleChangeHight = evt => {
        this.setState({
            hight: evt.target.value
        })
    }

    handleChangeWeight = evt => {
        this.setState({
            weight: evt.target.value
        })
    }

    handleSummit = () =>{
        let bd = moment(this.state.birthday).format("YYYY-MM-DD")
        let updateData = {
            NickName: this.state.nickname,
            Name: this.state.fullname,
            Mobile: this.state.tel,
             Email: this.state.email,
            Birthday: bd+"T00:00:00.0000Z",
            Height: parseInt(this.state.hight),
            Weight: parseInt(this.state.weight)
        }
        console.log(updateData);
        
        let pathName = "information"
        this.props.updateCV(updateData,this.props.cv.cv.id,pathName)
    }
    
    Cancel = () => {
        window.location.href ="/joblist"
    } 


    render() {
        return (
            <div className="BG">
                <div className="card-center">
                    <div className="card-body">
                        <h5>ทำความรู้จักกันก่อน</h5>
                        <img className="logo" src={logoCreateUser} alt="logoCreateUser" />
                    </div>
                </div>
                    <form>
                        <div className="form-group col-md-6">
                            <label>ชื่อเล่น<span>*</span></label>
                            <input className="form-control"
                                type="text"
                                value={this.state.nickname}
                                onChange={this.handleChangeNickname}
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label>ชื่อ - นามสกุล<span>*</span></label>
                            <input className="form-control"
                                type="text"
                                value={this.state.fullname}
                                onChange={this.handleChangeFullname}
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label>เบอร์โทรศัพท์<span>*</span></label>
                            <input className="form-control"
                                value={this.state.tel}
                                onChange={this.handleChangeTel}
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label>อีเมล<span>*</span></label>
                            <input className="form-control"
                                type="email"
                                aria-describedby="emailHelp"
                                value={this.state.email}
                            />
                        </div>
                        <div className="form-group col-md-6">
                            <label>วัน เดือน ปีเกิด<span>*</span></label>
                            <div>
                                <DatePicker
                                    selected={this.state.birthday}
                                    onChange={this.handleChangeBirthday}
                                />
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>ส่วนสูง<span>*</span></label>
                                <input className="form-control"
                                    type="text"
                                    value={this.state.hight}
                                    onChange={this.handleChangeHight}
                                />
                            </div>
                            <div className="form-group col-md-6">
                                <label>น้ำหนัก<span>*</span></label>
                                <input className="form-control"
                                    type="text"
                                    value={this.state.weight}
                                    onChange={this.handleChangeWeight}
                                />
                            </div>
                        </div>

                        <div className="btn-center">
                            <button type="button" className="btn btn-resubmit" onClick={this.Cancel}>ย้อนกลับ</button>
                            <button type="button" className="btn btn-submit" onClick={this.handleSummit}>ลุยต่อหน้าถัดไป</button>
                        </div>

                    </form>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
       
        loadCV: () => dispatch(loading()) ,
        updateCV: (updateData,userID,pathName) => dispatch(updateCV(updateData,userID,pathName))

    }
}

const mapStateToProps = reduxState => {
    return {
        cv: reduxState.cv
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CreateUser)