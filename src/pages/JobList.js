import React from 'react'
import '../style/joblist.css'
import logojoblist from '../img/AW-joblist.png'
import { connect } from 'react-redux'
import { jobList } from '../redux/action/job'
import { loading,updateCV } from '../redux/action/cv'

class JobList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectJob: "",
            jobID:"",
            
        }
    }

    async componentWillMount() {
        await this.props.loadCV()
        this.setState({
            jobID:this.props.cv.cv.JobID,
            selectJob:this.props.cv.cv.Job,
        })
    }

    async componentDidMount() {
        await this.props.job()
    }

    selectJob = async (e) => {
        let indexJob = e.target.value

        console.log(indexJob);
        
       let targetJob = this.props.jobList.filter((item)=>{
            return item.ID == indexJob
        })
        console.log(targetJob);
        

       await this.setState({
            selectJob: targetJob[0].Name,
            jobID: targetJob[0].ID
        })
        console.log(this.state.selectJob);
    }

    handleSummit = () => {
        let updateData = {
            Job: this.state.selectJob,
            JobID: this.state.jobID,
            
        }
        console.log(updateData);

        let pathName = "createuser"
         this.props.updateCV(updateData,this.props.cv.cv.id,pathName)
    }

    render() {
        if (this.props.jobList.length < 1) {
            return null
        }

        return (
            <div className="BG">
                <div className="card-center">
                    <div className="card-body">
                        <h5>ตำแหน่งงานที่ต้องการสมัคร</h5>
                        <img className="logo" src={logojoblist} alt="AWjoblist" />   
                    </div>
                    
                </div>

                <div className="form-select">
                    <label>ตำแหน่งงาน</label>
                    <select className="form-control form-selectjob"
                        name="selectJob"
                        onChange={(e) => this.selectJob(e)}
                        value={this.state.jobID}
                    >
                        <option></option>
                        {
                            this.props.jobList.map((item, index) => {
                                return (
                                    <option key={index} value={item.ID}>{item.Name}</option>
                                )
                            })
                        }
                    </select>
                    <div className="btn-center">
                     <button type="button" className="btn btn-submit" onClick={this.handleSummit}>เตรียมพร้อมสมัครงาน</button>
                </div>
                </div>
                
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        job: () => dispatch(jobList()),
        loadCV: () => dispatch(loading()) ,
        updateCV: (updateData,userID,pathName) => dispatch(updateCV(updateData,userID,pathName))

    }
}

const mapStateToProps = reduxState => {
    return {
        jobList: reduxState.job,
        selectJob: reduxState.job,
        cv: reduxState.cv
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JobList)