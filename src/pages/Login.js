import React from 'react'
import '../style/login.css'
import logo from '../img/LogoappPjoin.png'
import AWlogin from '../img/AW-login.png'
import { connect } from 'react-redux'
import {login} from '../redux/action/cv'

class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            email: "",
            code: "",
        }
    }

    handleChangeEmail = evt =>{
        this.setState({
            email:evt.target.value,
        })
    }

    handleChangePassword = evt =>{
        this.setState({
            code:evt.target.value,
        })
    }

    handleSummit = () => {
        this.props.login(this.state.email,this.state.code)
       // window.location.href = "/joblist"
    }

    render() {
        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col col-lg-12">
                            <div className="card">
                                <img className="logo-login" src={logo} alt="logo"/>
                                <img src={AWlogin} alt="logo-login"/>
                                <form>
                                    <div className=".form-login">
                                      <div className="form-group">
                                        <input type="email"
                                            className="form-control"
                                            placeholder="อีเมล"
                                            value={this.state.email}
                                            onChange = {this.handleChangeEmail}
                                        />
                                    </div>

                                    <div className="form-group">
                                        <input 
                                            className="form-control"
                                            placeholder="รหัสสมัครงาน (รหัสที่ได้รับจากอีเมล)"
                                            value={this.state.code}
                                            onChange = {this.handleChangePassword}
                                        />
                                    </div>  
                                    </div>
                                    
                                    <button type="button" className="btn btn-lg" onClick={this.handleSummit}>เข้าสู่ระบบ</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return{
        login: (email,code) => dispatch(login(email,code))
    }
}

const  mapStateToProps = reduxState => {
    return{
      cv: reduxState.cv
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Login)