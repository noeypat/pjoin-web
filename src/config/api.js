const hostDev = 'http://206.189.145.21:8000/v1'
const hostProduction = 'http://206.189.145.21:8000/v1'
export const hostName = ''
const production = process.env.NODE_ENV === 'production';
const api = {
  host: production ? hostProduction : hostDev,
  imageUploadHost : "https://secure-media.saranros.com/upload",

  imageHost : "https://secure-media.saranros.com/image/960X720/",
  imageAvatar : `https://secure-media.saranros.com/image/300X300/`


}
export default api
