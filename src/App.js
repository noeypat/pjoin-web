import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from './pages/Login';
import JobList from './pages/JobList'
import CreateUser from './pages/CreateUser'
import userInformation from './pages/Information'
import Address from './pages/Address'
import ContactPerson from './pages/ContactPerson'

function App() {
  return (
    <Router>
      <div>

        <Route exact path="/login" component={Login} />
        <Route exact path="/joblist" component={JobList} />
        <Route exact path="/createuser" component={CreateUser} />
        <Route exact path="/information" component={userInformation} />
        <Route exact path="/address" component={Address} />
        <Route exact path="/contactperson" component={ContactPerson} />
       
      </div>
    </Router>
  );
}

export default App;
